#include "File.h"

std::unique_ptr<std::string> File::get_input_from(std::string path) {
    std::filesystem::path file_path { path };
    std::string buf { };
    read_file(path, &buf);
    return std::make_unique<std::string>(buf);
}

void File::read_file(std::filesystem::path path, std::string *buf) {
    std::ifstream ReadFile(std::filesystem::absolute(path));

    if (!ReadFile.is_open()) {
        std::cerr << "Couldn't open file: " << std::filesystem::absolute(path);
        return;
    }

    std::string line {""};

    while (std::getline(ReadFile, line))
        *buf += line + "\n";

    ReadFile.close();
}


