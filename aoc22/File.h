//
// Created by Joshua Kobes on 01/12/2022.
//

#ifndef AOC22_FILE_H
#define AOC22_FILE_H

#include <iostream>
#include <fstream>
#include <filesystem>
#include <string>
#include <memory>

class File {
public:
    static std::unique_ptr<std::string> get_input_from(std::string path);
private:
    static void read_file(std::filesystem::path path, std::string *buf);
};


#endif //AOC22_FILE_H
